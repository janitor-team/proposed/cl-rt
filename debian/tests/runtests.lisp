(require "asdf")

(let* ((tmpdir (uiop:getenv "AUTOPKGTEST_TMP"))
       (asdf:*user-cache* tmpdir)) ; Store FASL in some temporary dir
  ;; rt-test.lisp asks for a temporary filename on *standard-input*
  (with-input-from-string (*standard-input* (format nil "\"~A/tmpfile\"" tmpdir))
    (load #p"/usr/share/common-lisp/source/rt/rt-test.lisp")))

(unless (do-tests)
  (uiop:quit 1))
